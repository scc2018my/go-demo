package main

import (
	"fmt"
	"gitee.com/scc2018my/go-demo/hello"
)

func main()  {

	//hello world
	fmt.Println(hello.Hello())

	//iota
	/*const (
		a = iota
		b
		c
	)
	fmt.Println(a, b, c)*/

	//类型转换
	/*type Integer int
	var a Integer
	var b int
	a = 5
	b = int(a)
	fmt.Println(b)*/

	//指针
	/*experiment.PointerTest()*/

	//if子句
	/*if a := 10; a < 20 {
		fmt.Println("a小于20")
	} else {
		fmt.Println("a大于20")
	}*/

	//类型switch语句
	/*experiment.SwitchTest()*/

	//goto语句
	/*experiment.GotoTest()*/

	//函数
	/*experiment.FuncTest()*/

	//defer语句
	/*fmt.Println(experiment.DeferA())
	fmt.Println(experiment.DeferB())
	experiment.DeferTest()*/

	//panic、recover
	/*experiment.PanicRecoverTest()*/

	//数组
	/*arr1 := [...]int{2, 4, 6, 8, 10}
	fmt.Println(len(arr1))
	arr2 := new([2]string)
	arr2[0] = "hello"
	arr2[1] = "world"
	fmt.Println(arr2)*/

	//切片
	/*slice1 := make([]string, 2)
	slice1[0] = "hello"
	slice1[1] = "world"
	for _, val := range slice1 {
		fmt.Println(val)
	}
	slice2 := []int{6, 7}
	slice2 = append(slice2, 8)
	fmt.Println(slice2)
	slice3 := []int{10, 20, 30, 40, 50}
	newSlice := slice3[1:3]
	fmt.Println(newSlice)
	newSlice[1] = 35
	fmt.Println(newSlice)
	fmt.Println(slice3)*/

	//映射
	/*map1 := make(map[int]string)
	map1[1] = "hello"
	map1[2] = "world"
	fmt.Println(map1)
	map2 := map[string]int{"first": 3, "second": 5}
	for key, val := range map2 {
		fmt.Println(key, val)
	}
	delete(map1, 1)
	fmt.Println(map1)*/

	//结构体
	/*experiment.StructTest()*/

	//方法
	/*point := &experiment.Point{X: 1, Y: 2}
	fmt.Println(point.Computed(3, 4))
	var i experiment.Integer
	fmt.Println(i.Add(2, 3))*/

	//面向对象
	/*person := experiment.NewPerson("scott", 18)
	fmt.Println(person.GetName())
	fmt.Println(person.GetAge())
	person.SetName("张三")
	person.SetAge(20)
	fmt.Println(person.GetName())
	fmt.Println(person.GetAge())*/

	//继承
	/*mobile := new(experiment.Mobile)
	fmt.Println(mobile.Call())
	fmt.Println(mobile.TakePicture())
	fmt.Println(mobile.SendMessage())*/

	//接口
	/*var shape experiment.Shape
	square := new(experiment.Square)
	square.Side = 5
	shape = square
	fmt.Println(shape.Area())
	fmt.Println(shape.Perimeter())
	rectangle := &experiment.Rectangle{Length: 3, Width: 4}
	shape = rectangle
	fmt.Println(shape.Area())
	fmt.Println(shape.Perimeter())*/

	//多态
	/*cat := new(experiment.Cat)
	fmt.Println(experiment.Eat(cat))
	dog := new(experiment.Dog)
	fmt.Println(experiment.Eat(dog))*/

	//反射
	/*experiment.ReflectTest()*/
}


