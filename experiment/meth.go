/**
 * 方法
 */
package experiment

type Point struct {
	X, Y int
}

func (p *Point) Computed(a, b int) int {
	return p.X * a + p.Y * b
}

type Integer int

func (i Integer) Add(a, b int) int {
	return a + b
}
