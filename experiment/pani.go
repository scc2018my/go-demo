/**
 * panic、recover
 */
package experiment

func PanicRecoverTest()  {
	defer func() {
		recover()
	}()
	panic("发生错误")
}