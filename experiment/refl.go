/*
 * 反射
 */
package experiment

import (
	"fmt"
	"reflect"
)

type T struct {
	A int
	B string
}

func ReflectTest() {
	var x float64 = 1.2
	v := reflect.ValueOf(x)
	fmt.Println("v的类型", v.Type())
	fmt.Println("v:", v.CanSet())
	v = reflect.ValueOf(&x)
	fmt.Println("v的类型", v.Type())
	fmt.Println("v:", v.CanSet())
	v = v.Elem()
	fmt.Println("v:", v.CanSet())
	v.SetFloat(2.1)
	fmt.Println(v.Interface())

	t := &T{23, "hello"}
	fmt.Println("t的初始值是:", t)
	s := reflect.ValueOf(t).Elem()
	for i := 0; i < s.NumField(); i++ {
		f := s.Field(i)
		fmt.Printf("%d: %s %s = %v\n", i, s.Type().Field(i).Name, f.Type(), f.Interface())
	}
	s.Field(0).SetInt(77)
	s.Field(1).SetString("world")
	fmt.Println("t现在的值是:", t)
}
