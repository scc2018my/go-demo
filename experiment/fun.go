/**
 * 函数
 */
package experiment

import (
	"fmt"
)

func isEven(v int) bool  {
	if v % 2 == 0 {
		return true
	}
	return false
}

type booFunc func(int) bool

func filter(a []int, ff booFunc) (b []int) {
	for _, val := range a {
		if ff(val) {
			b = append(b, val)
		}
	}
	return
}

func sum(a ...int) int {
	var s int
	for _, val := range a {
		s += val
	}
	return s
}

func getSequence() func() int {
	i := 0
	return func() int {
		i++
		return i
	}
}

func FuncTest()  {
	sli := []int{1, 2, 3, 4, 5, 6, 7, 8}
	fmt.Println(filter(sli, isEven))

	fmt.Println(sum(1, 2, 3))

	fmt.Println(func (a int) int {
		s := 0
		for i := 1; i <= a; i++ {
			s += i
		}
		return s
	}(100))

	nextNumber1 := getSequence()
	fmt.Println(nextNumber1())
	fmt.Println(nextNumber1())
	nextNumber2 := getSequence()
	fmt.Println(nextNumber2())
	fmt.Println(nextNumber2())
}