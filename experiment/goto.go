/**
 * goto语句
 */
package experiment

import "fmt"

var i int

func GotoTest()  {
	for {
		fmt.Println(i)
		i++
		if i > 2 {
			goto Break
		}
	}
	Break:
		fmt.Println("break")
}