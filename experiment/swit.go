/**
 * 类型switch语句
 */
package experiment

import "fmt"

var x interface{}

func SwitchTest()  {
	x = 1
	switch x.(type) {
	case nil:
		fmt.Println("x的类型是nil")
	case int:
		fmt.Println("x的类型是int")
	case float64:
		fmt.Println("x的类型是float64")
	case bool:
		fmt.Println("x的类型是bool")
	case string:
		fmt.Println("x的类型是string")
	default:
		fmt.Println("x的类型未知")
	}
}
