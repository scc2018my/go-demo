/**
 * 接口
 */
package experiment

import (
	"fmt"
)

type A struct {
	name string
	age int
}

type B struct {
	city string
}

type C struct {
	A
	B
	int
}

func StructTest()  {
	var a = new(A)
	a.name = "scott.song"
	a.age = 18
	fmt.Println(a)

	b := &B{"深圳"}
	fmt.Println(b)

	c := C{A{age: 20, name: "张三"}, B{city: "深圳"}, 1}
	fmt.Println(c)
}