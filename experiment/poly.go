/*
 * 多态
 */
package experiment

type animal interface {
	eat() string
}

type Cat struct {

}

func (c *Cat) eat() string {
	return "猫吃鱼"
}

type Dog struct {

}

func (d *Dog) eat() string {
	return "狗吃骨头"
}

func Eat(a animal) string {
	return a.eat()
}