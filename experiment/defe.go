/**
 * defer语句
 */
package experiment

import "fmt"

func DeferA() int {
	var i int
	defer func() {
		i++
		fmt.Println("defer2:", i)
	}()
	defer func() {
		i++
		fmt.Println("defer1:", i)
	}()

	return i
}

func DeferB() (i int) {
	defer func() {
		i++
		fmt.Println("defer2:", i)
	}()
	defer func() {
		i++
		fmt.Println("defer1:", i)
	}()

	return i
}

var a = 0
var b = 0

func printA() {
	fmt.Println(a)
}

func printB(b int) {
	fmt.Println(b)
}

func DeferTest()  {
	for ; a < 5; a++ {
		defer printA()
	}

	for ; b < 5; b++ {
		defer printB(b)
	}
}