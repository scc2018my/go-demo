/**
 * 指针
 */
package experiment

import "fmt"

func swap(a, b *int) {
	*a, *b = *b, *a
}

func PointerTest() {
	a := 5
	b := 10
	fmt.Println("交换前a和b分别为：", a, b)
	swap(&a, &b)
	fmt.Println("交换后a和b分别为：", a, b)
}
