/**
 * 继承
 */
package experiment

type camera struct {

}

func (c *camera) TakePicture() string {
	return "拍照"
}

type phone struct {

}

func (p *phone) Call() string {
	return "打电话"
}

type Mobile struct {
	camera
	phone
}

func (m *Mobile) SendMessage() string  {
	return "发短信"
}