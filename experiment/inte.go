/*
 * 接口
 */
package experiment

type Shape interface {
	Area() int
	Perimeter() int
}

type Square struct {
	Side int
}

func (s *Square) Area() int {
	return s.Side * s.Side
}

func (s *Square) Perimeter() int {
	return s.Side * 4
}

type Rectangle struct {
	Length int
	Width  int
}

func (r *Rectangle) Area() int {
	return r.Length * r.Width
}

func (r *Rectangle) Perimeter() int {
	return (r.Length + r.Width) * 2
}
