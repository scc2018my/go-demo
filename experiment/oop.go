/*
 * 面向对象
 */
package experiment

type person struct {
	name string
	age int
}

func NewPerson(name string, age int) *person {
	return &person{name, age}
	//p := new(person)
	//p.name = name
	//p.age = age
	//return p
}

func (p *person) SetName(name string) {
	p.name = name
}

func (p *person) SetAge(age int)  {
	p.age = age
}

func (p *person)GetName() string {
	return p.name
}

func (p *person)GetAge() int {
	return p.age
}